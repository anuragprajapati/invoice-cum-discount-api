package com.invoice.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.invoice.constants.ItemType;
import com.invoice.constants.UserType;
import com.invoice.dto.Invoice;
import com.invoice.dto.Item;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {

	private static final Logger log = LogManager.getLogger(InvoiceServiceImpl.class);

	@Override
	public Invoice generateInvoice(Invoice invoice) {
		final String method = "generateInvoice(invoice)";
		
		log.debug("");
		log.debug("=====================================================");
		log.debug("Entry - {} with {}", method, invoice);

		if (null != invoice.getUser() && CollectionUtils.isNotEmpty(invoice.getItems())) {

			log.debug("User - {} and ItemsList - {}", invoice.getUser(), invoice.getItems());

			double totalGroceryItemsPrice = 0;
			double totalNonGroceryItemsPrice = 0;

			for (Item item : invoice.getItems()) {
				if (ItemType.GROCERY_ITEM.equals(item.getItemCategory())) {
					totalGroceryItemsPrice += item.getQuantity() * item.getPrice();
					log.debug("Item - {}", item);
				} else {
					totalNonGroceryItemsPrice += item.getQuantity() * item.getPrice();
					log.debug("Item - {}", item);
				}
			}

			log.debug("totalGroceryItemsPrice - {}", totalGroceryItemsPrice);
			log.debug("totalNonGroceryItemsPrice - {}", totalNonGroceryItemsPrice);

			if (UserType.EMPLOYEE.equals(invoice.getUser().getUserType())) {
				totalNonGroceryItemsPrice -= (totalNonGroceryItemsPrice * 30) / 100;
				log.debug("UserType - {}", invoice.getUser().getUserType());
				log.debug("totalNonGroceryItemsPrice - {}", totalNonGroceryItemsPrice);
			} else if (UserType.AFFILIATE.equals(invoice.getUser().getUserType())) {
				totalNonGroceryItemsPrice -= (totalNonGroceryItemsPrice * 10) / 100;
				log.debug("UserType - {}", invoice.getUser().getUserType());
				log.debug("totalNonGroceryItemsPrice - {}", totalNonGroceryItemsPrice);
			} else {
				LocalDate dateBefore2YearsFromTodaysDate = LocalDate.now().minus(2, ChronoUnit.YEARS);
				boolean isDateDifference = invoice.getUser().getUserCreatedDate()
						.isBefore(dateBefore2YearsFromTodaysDate);

				if (isDateDifference) {
					totalNonGroceryItemsPrice -= (totalNonGroceryItemsPrice * 5) / 100;
				}

				log.debug("UserType - null");
				log.debug("Has User been a customer forover 2 years - {}", isDateDifference);
				log.debug("totalNonGroceryItemsPrice - {}", totalNonGroceryItemsPrice);
			}

			double totalFinalPrice = totalGroceryItemsPrice + totalNonGroceryItemsPrice;
			int countsOf100s = (int) totalFinalPrice / 100;
			totalFinalPrice -= countsOf100s * 5;
			invoice.setTotalFinalPrice(totalFinalPrice);

			log.debug("countsOf100s - {}", countsOf100s);
			log.debug("totalFinalPrice - {}", totalFinalPrice);
		}

		log.debug("Exit - {} with {}", method, invoice);
		log.debug("=====================================================");
		log.debug("");
		return invoice;
	}

}
