package com.invoice.service;

import com.invoice.dto.Invoice;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public interface InvoiceService {
	public Invoice generateInvoice(Invoice invoice);
}
