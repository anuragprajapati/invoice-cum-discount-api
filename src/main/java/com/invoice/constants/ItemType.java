package com.invoice.constants;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public enum ItemType {

	GROCERY_ITEM, NON_GROCERY_ITEM;

}
