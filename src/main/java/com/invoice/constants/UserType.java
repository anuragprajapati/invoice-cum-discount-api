package com.invoice.constants;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public enum UserType {

	EMPLOYEE, AFFILIATE;

}