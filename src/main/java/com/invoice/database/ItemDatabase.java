package com.invoice.database;

import java.util.Arrays;
import java.util.List;

import com.invoice.constants.ItemType;
import com.invoice.dto.Item;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public class ItemDatabase {

	public static List<Item> getListOfItems() {
		return Arrays.asList(new Item(6789l, "Suger", ItemType.GROCERY_ITEM, 70.0, 0),
				new Item(9635l, "Shampoo", ItemType.NON_GROCERY_ITEM, 50.0, 0));

	}

}
