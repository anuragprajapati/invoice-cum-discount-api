package com.invoice.database;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.invoice.constants.UserType;
import com.invoice.dto.User;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public class UserDatabase {

	public static User getUser(Long userId) {

		// need to add different dates
		Map<Long, User> userMap = new HashMap<>();

		userMap.put(1111l, new User(1111l, "Anurag", UserType.EMPLOYEE, LocalDate.parse("2020-06-01")));
		userMap.put(2222l, new User(2222l, "Ankit", UserType.AFFILIATE, LocalDate.parse("2020-11-01")));
		userMap.put(3333l, new User(3333l, "Shital", null, LocalDate.parse("2018-11-01")));
		userMap.put(4444l, new User(4444l, "Heena", null, LocalDate.parse("2021-04-01")));

		return userMap.get(userId);
	}

}
