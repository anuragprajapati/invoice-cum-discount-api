package com.invoice.dto;

import com.invoice.constants.ItemType;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public class Item {

	private Long itemId;
	private String itemName;
	private ItemType itemCategory;
	private double price;
	private int quantity;

	public Item() {
		super();
	}

	public Item(Long itemId, String itemName, ItemType itemCategory, double price, int quantity) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemCategory = itemCategory;
		this.price = price;
		this.quantity = quantity;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public ItemType getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(ItemType itemCategory) {
		this.itemCategory = itemCategory;
	}

	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemCategory=" + itemCategory + ", price="
				+ price + ", quantity=" + quantity + "]";
	}

}
