package com.invoice.dto;

import java.time.LocalDate;

import com.invoice.constants.UserType;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public class User {

	private Long userId;
	private String userName;
	private UserType userType;
	private LocalDate userCreatedDate;

	public User(Long userId, String userName, UserType userType, LocalDate userCreatedDate) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userType = userType;
		this.userCreatedDate = userCreatedDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public LocalDate getUserCreatedDate() {
		return userCreatedDate;
	}

	public void setUserCreatedDate(LocalDate userCreatedDate) {
		this.userCreatedDate = userCreatedDate;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", userType=" + userType + ", userCreatedDate="
				+ userCreatedDate + "]";
	}

}
