package com.invoice.dto;

import java.time.LocalDate;
import java.util.List;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
public class Invoice {

	private Long invoiceId;
	private LocalDate invoiceDate;
	private List<Item> items;
	private User user;
	private double totalFinalPrice;

	public Invoice(Long invoiceId, LocalDate invoiceDate, List<Item> items, User user, double totalFinalPrice) {
		super();
		this.invoiceId = invoiceId;
		this.invoiceDate = invoiceDate;
		this.items = items;
		this.user = user;
		this.totalFinalPrice = totalFinalPrice;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getTotalFinalPrice() {
		return totalFinalPrice;
	}

	public void setTotalFinalPrice(double totalFinalPrice) {
		this.totalFinalPrice = totalFinalPrice;
	}

	@Override
	public String toString() {
		return "Invoice [invoiceId=" + invoiceId + ", invoiceDate=" + invoiceDate + ", items=" + items + ", user="
				+ user + ", totalFinalPrice=" + totalFinalPrice + "]";
	}

}
