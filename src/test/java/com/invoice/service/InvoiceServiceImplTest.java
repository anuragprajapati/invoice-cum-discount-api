package com.invoice.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.invoice.constants.UserType;
import com.invoice.database.ItemDatabase;
import com.invoice.database.UserDatabase;
import com.invoice.dto.Invoice;
import com.invoice.dto.Item;
import com.invoice.dto.User;

/**
 * 
 * @author Anurag.Prajapati
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceImplTest {

	private static final Logger log = LogManager.getLogger(InvoiceServiceImplTest.class);

	@InjectMocks
	private InvoiceServiceImpl invoiceServiceTest;

	@Test
	public void test_generateInvoice_HappyPath_When_UserIsEmployee() {

		log.debug("Testcase - When UserType is EMPLOYEE");

		double expectedTotalFinalPrice = 235.0;
		User user = UserDatabase.getUser(1111l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), getItemsListWithUpdateQuantity(2, 3),
				user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertEquals(UserType.EMPLOYEE, invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);
		assertEquals(LocalDate.parse("2020-06-01"), invoiceAfterBillGeneration.getUser().getUserCreatedDate());

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	@Test
	public void test_generateInvoice_HappyPath_When_UserIsAffiliate() {

		log.debug("Testcase - When UserType is AFFILIATE");

		double expectedTotalFinalPrice = 265.0;
		User user = UserDatabase.getUser(2222l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), getItemsListWithUpdateQuantity(2, 3),
				user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertEquals(UserType.AFFILIATE, invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);
		assertEquals(LocalDate.parse("2020-11-01"), invoiceAfterBillGeneration.getUser().getUserCreatedDate());

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	/**
	 * When user has been a customer for over 2 years
	 */
	@Test
	public void test_generateInvoice_HappyPath_When_UserIsNotEmployeeOrNotAffiliate_1() {
		log.debug("Testcase - When UserType is not EMPLOYEE or not AFFILIATE and user has been a customer for over 2 years");

		double expectedTotalFinalPrice = 272.5;
		User user = UserDatabase.getUser(3333l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), getItemsListWithUpdateQuantity(2, 3),
				user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertNull(invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);
		assertEquals(LocalDate.parse("2018-11-01"), invoiceAfterBillGeneration.getUser().getUserCreatedDate());

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	/**
	 * When user has not been a customer for over 2 years
	 */
	@Test
	public void test_generateInvoice_HappyPath_When_UserIsNotEmployeeOrNotAffiliate_2() {
		log.debug("Testcase - When UserType is not EMPLOYEE or not AFFILIATE and user has not been a customer for over 2 years");

		double expectedTotalFinalPrice = 280.0;
		User user = UserDatabase.getUser(4444l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), getItemsListWithUpdateQuantity(2, 3),
				user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertNull(invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);
		assertEquals(LocalDate.parse("2021-04-01"), invoiceAfterBillGeneration.getUser().getUserCreatedDate());

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	@Test
	public void test_generateInvoice_When_UserIsNotAvailable() {
		log.debug("Testcase - When User is not available or user is null");

		double expectedTotalFinalPrice = 0.0;
		User user = null;
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), getItemsListWithUpdateQuantity(2, 3),
				user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertNull(invoiceAfterBillGeneration.getUser());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);

		log.debug("User - null");
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - null");
		log.debug("");
	}

	@Test
	public void test_generateInvoice_When_ItemsList_Null() {
		log.debug("Testcase - When ItemList is null");

		double expectedTotalFinalPrice = 0.0;
		User user = UserDatabase.getUser(3333l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), null, user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertNull(invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	@Test
	public void test_generateInvoice_When_Items_Empty() {
		log.debug("Testcase - When ItemList is Empty");

		double expectedTotalFinalPrice = 0.0;
		User user = UserDatabase.getUser(4444l);
		Invoice invoiceBeforeBillGeneration = new Invoice(8456l, LocalDate.now(), Arrays.asList(), user, 0.0);

		// execute
		Invoice invoiceAfterBillGeneration = invoiceServiceTest.generateInvoice(invoiceBeforeBillGeneration);

		assertNull(invoiceAfterBillGeneration.getUser().getUserType());
		assertEquals(expectedTotalFinalPrice, invoiceAfterBillGeneration.getTotalFinalPrice(), 0.001);

		log.debug("UserType - {}", invoiceAfterBillGeneration.getUser().getUserType());
		log.debug("TotalFinalPrice - {}", invoiceAfterBillGeneration.getTotalFinalPrice());
		log.debug("UserCreatedDate - {}", invoiceAfterBillGeneration.getUser().getUserCreatedDate());
		log.debug("");
	}

	private List<Item> getItemsListWithUpdateQuantity(int iteamOneQty, int iteamTwoQty) {
		List<Item> itemsList = ItemDatabase.getListOfItems();

		Item item1 = itemsList.get(0);
		item1.setQuantity(iteamOneQty);

		Item item2 = itemsList.get(1);
		item2.setQuantity(iteamTwoQty);

		List<Item> itemsListWithUpdateQuantity = new ArrayList<>();
		itemsListWithUpdateQuantity.add(item1);
		itemsListWithUpdateQuantity.add(item2);

		return itemsListWithUpdateQuantity;
	}

}
