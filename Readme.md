## Getting Started

## invoice-cum-discount-api

## Description:

## Required tools and technology
* Java8
* Eclipse / Sts
* Spring Boot
* Jococo - For code coverage
* JUnit and Mockito
* Log4j

* It is a invoice-cum-discount generation api with discounts based on the below conditions.

* On a retail website, the following discounts apply:
* 1. If the user is an employee of the store, he gets a 30% discount.
* 2. If the user is an affiliate of the store, he gets a 10% discount.
* 3. If the user has been a customer for over 2 years, he gets a 5% discount.
* 4. For every $100 on the bill, there would be a $5 discount (e.g. for $990, you get $ 45 as a discount).
* 5. The percentage-based discounts do not apply to groceries.
* 6. A user can get only one of the percentage-based discounts on a bill.

* -> It contains three model classes
* 1. User - Contains user related data members and getters/setters
* private Long userId;
* private String userName;
* private UserType userType;
* private LocalDate userCreatedDate;
	
* 2. Item - Contains item related data members and getters/setters
* private Long itemId;
* private String itemName;
* private ItemType itemCategory;
* private double price;
* private int quantity;
	
* 3. Invoice - Contains invoice related data members getters/setters
* private Long invoiceId;
* private LocalDate invoiceDate;
* private List<Item> items;
* private User user;
* private double totalFinalPrice;
	
* -> It contains two enums
* 1. UserType enum - EMPLOYEE, AFFILIATE
* 2. ItemType enum - GROCERY_ITEM, NON_GROCERY_ITEM

* -> It contains two classes that behave like local database
* 1. UserDatabase - public static User getUser(Long userId)
* 2. ItemDatabase - public static List<Item> getListOfItems()

## Note: 
* By calling the above static methods we will get the predefined users and items list.

* -> It contains one InvoiceService interface and one implementation for this interface as InvoiceServiceImpl

* 1. InvoiceService - public Invoice generateInvoice(Invoice invoice);

## Note: 
* When we call generateInvoice method by passing the invoice object, then it will process the items list based on user type and then applies the discounts as mentions in the above list.

* -> For a generation of the code-coverage report please follow the below steps.

* right-click on the project -> Coverage As -> JUnit Test -> Coverage window will be open, in which you can see the coverage for each and every class present in the project.

* -> It contains one InvoiceServiceImplTest test class, in which there are test cases based on multiple scenarios and if you run this class then you will see the results for different scenarios covered as part of this test class.
